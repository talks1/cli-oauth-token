const axios = require( "axios" )
const crypto = require( "crypto" )
const express = require('express')
const open = require( "open" )
const querystring = require( "querystring" )
const { v4 : uuid}  = require( "uuid" )
const jwt_decode = require("jwt-decode")
const Debug = require("debug")

const debug = Debug('auth')

const base64url = str => {
 return str.replace( /\+/g, "-" ).replace( /\//g, "_" ).replace( /=+$/, "" )
}

 module.exports = ( config ) => { 
  
  debug(config)
  const { oauthUrl, clientId, scopes, redirectUrl , serverPort, openidConfigUrl} = config
  if ( !oauthUrl || !clientId || !scopes || !redirectUrl|| !serverPort || !openidConfigUrl) {
   throw new Error( "OAuth URL, client ID, scopes, and server port are required." )
  }

  // code verifier must be a random string with a minimum of 43 characters
  const codeVerifier = uuid() + uuid()
  const redirectUri = redirectUrl

  const buildAuthorizeUrl = ( codeChallenge ) => {
    const data = {
      client_id: clientId,
      response_type: "code",
      response_mode:"query",
      scope: scopes,
      redirect_uri: redirectUri,
      state: uuid(),
      code_challenge_method: "S256",
      code_challenge: codeChallenge
    }
    const params = querystring.stringify( data )
    const authorizeUrl = `${oauthUrl}/authorize?${params}`
    debug(authorizeUrl)
    return authorizeUrl
  }

  const getOAuthConfig = async (openidConfgUrl) => {
    try {        
      const res = await axios.get( openidConfgUrl )
      debug(res.data)
      return res.data
    } catch ( err ) {
      console.log( "error openid", err ) // eslint-disable-line no-console
      throw err
    }
  }

  const getUserInfo = (accessToken) => {   
      const info = jwt_decode(accessToken)         
      return info   
  }

  const getToken = async (openidConfig,code) => {
    try {
      const request = {
        grant_type: "authorization_code",
        redirect_uri: redirectUri,
        client_id: clientId,
        code,
        code_verifier: codeVerifier
      }
      const headers = {
        "Origin": redirectUrl,
        "User-Agent": "Mozilla/5.0 (X11 Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36"
      }
      const url = openidConfig.token_endpoint //`${oauthUrl}/oauth/token`
      const data = querystring.stringify( request )     
      const res = await axios.post( url, data , {headers:headers})
      debug(res.data)
      return res.data
    } catch ( err ) {
      console.log( "error getting token", err ) // eslint-disable-line no-console
      throw err
    }
  }
    // Start server and begin auth flow
  const executeAuthFlow = () => {
    return new Promise( async ( resolve, reject ) => {

      const openidConfig = await getOAuthConfig(openidConfigUrl)    
      
      const app = express()
      const port = 3000
      const server = app.listen(port, () => {
        debug(`Example app listening at http://localhost:${port}`)
      })

      app.get('/', async (request, res) => {
        try { 
        const code = request.query.code                         
        const token = await getToken( openidConfig,code )              
        const userInfo = getUserInfo( token.access_token )                         
        const response = { token, userInfo }
        resolve( response )       
        res.send(`Thank you, ${userInfo.unique_name}. You can close this tab.`)
        } catch ( err ) {
          reject( err )
        } finally {
          server.close()
        } 
      })
      const codeChallenge = base64url( crypto.createHash( "sha256" ).update( codeVerifier ).digest( "base64" ) );
      const authorizeUrl = buildAuthorizeUrl( codeChallenge );
      debug(`Taking user to authorizeUrl ${authorizeUrl}`)
      open( authorizeUrl );
    } )
  }
  return {
   executeAuthFlow
 }
}