const chalk = require( "chalk" );
const dotenv = require( "dotenv" );
dotenv.config();
const Debug = require("debug")
const authClient = require( "./authClient" );
const debug = Debug("main")


const openidConfigUrl = `https://login.microsoftonline.com/${process.env.AUTH_TENANTID}/v2.0/.well-known/openid-configuration`
const authurl = `https://login.microsoftonline.com/${process.env.AUTH_TENANTID}/oauth2/v2.0`

const config = {
 openidConfigUrl: openidConfigUrl,
 oauthUrl: authurl,
 clientId: process.env.AUTH_CLIENTID,
 scopes: process.env.AUTH_SCOPES,
 redirectUrl: process.env.AUTH_REDIRECTURI,
 serverPort: process.env.SERVER_PORT, 
}

const main = async () => {
 try {
   const auth = authClient( config );
   const { token, userInfo } = await auth.executeAuthFlow();            
   debug( chalk.bold( `You ${userInfo.unique_name} have successfully authenticated your CLI application!` ) );
   debug(token)
 } catch ( err ) {
   console.log( chalk.red( err ) );
 }
}

main();