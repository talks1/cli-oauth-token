# CLI to acquire an authorization token

This CLI demonstrates how to authenticate in the browser to get an access token which can then be used for subsequent API calls.




## Set up


### Prequisites

- [Install nodejs](https://nodejs.org/en/download)

### Install Dependencies

Then install dependencies

```npm install```

## Running the application

To use the CLI just run 

```npm start```